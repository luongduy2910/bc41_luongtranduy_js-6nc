/**
 * Dữ liệu đầu vào là con số mà user nhập 
 * 
 * Xử lý : 
 * Xây dựng hàm để kiểm tra xem số đó có phải là số nguyên tố hay là không 
 * Xây dựng hàm để kiểm tra các con số từ 1 tới số user có phải số nguyên tố hay không bằng cách truyền đối số vào hàm đã xây dựng 
 * Nếu là số nguyên đó thì in ra 
 */




function kiemTraSoNguyenTo(n) {
    var flag = true ; 
    if (n < 2) {
        flag = false ; 
    }else if (n  == 2) {
        flag = true ; 
    }else if (n % 2 == 0) {
        flag = false ; 
    }else {
        for (var i =3 ; i <= Math.sqrt(n) ; i+=2) {
            if (n %i ==0) {
                flag = false  ; 
                break ; 
            }
        }
    }
    return flag ; 
}

document.getElementById('tinhsonguyento').addEventListener('click', function(){
    var numUser = document.getElementById('txt-number').value * 1 ; 
    var html = " " ;
    for (var i = 1 ; i <= numUser ;  i++) {
        if(kiemTraSoNguyenTo(i)) {
            html += i + " " ;
        }
    }
    document.getElementById('show-ketqua').innerHTML = html ; 
})
